//Alex Suvorov
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
using namespace std;

//defining class Expression
class Expression
{
    private:
        int p_operand1;
        int p_operand2;
        char p_operator_;

    public:
        Expression(int operand1, int operand2, char operator_);
        virtual string toString();
        virtual char getOpeator();
};

Expression::Expression(int operand1, int operand2, char operator_):
    p_operand1(operand1), p_operand2 (operand2), p_operator_ (operator_){}

string Expression::toString()
{
    int result;
    switch(p_operator_)
    {
        case '+': result = p_operand1 + p_operand2 ; break ;
        case '-': result = p_operand1 - p_operand2 ; break ;
        case '*': result = p_operand1 * p_operand2 ; break ;
        case '/': result = p_operand1 / p_operand2 ; break ;
        case '%': result = p_operand1 % p_operand2 ; break ;
    }
    stringstream ss;
    ss << "\t" << to_string(p_operand1) << "\t" << p_operator_ << "\t" << to_string(p_operand2) << "\t" << "=" << "\t" << result;
    return ss.str();
}

char Expression::getOpeator()
{
    return p_operator_;
}

//defining class NamedExpression
class NamedExpression : public Expression
{
    private:
        string p_exp_name;

    public:
        NamedExpression(string exp_name, int operand1, int operand2, char operator_);
        string toString();
};

NamedExpression::NamedExpression(string exp_name, int operand1, int operand2, char operator_):
    p_exp_name (exp_name), Expression::Expression (operand1, operand2, operator_){}

string NamedExpression::toString()
{
    stringstream ss;
    ss << Expression::toString() << " NAME(" << p_exp_name << ")";
    return ss.str();
}

//declaration of the further functions
void operator_funct(int operand1, int operand2, char operator_, int &intermediate_result,
                     int &tot_num_of_expr, int &num_plus, int &num_mult, int &num_minus, int &num_div, int &num_remaind);

void addiTion (int num_1,  int num_2, char operator_, int& result);
void subTraction (int num_1,  int num_2, char operator_, int& result);
void mulTiplication (int num_1,  int num_2, char operator_, int& result);
void diviSion (int num_1,  int num_2, char operator_, int& result);
void reMainder (int num_1,  int num_2, char operator_, int& result);

void resultChecker (int& larg_result,  int& small_result, int& intermediate_result);

vector<string> tempFunct(string string_, char operator_);
void print_vector(vector<Expression*> vect);
bool isNumber(string& str);

//core function of the code (command-driven program)
void ListNamedExpressions()
{
    cout << "Welcome to expression management program.";
    int operand1;
    int operand2;
    char operator_;

    int tot_num_of_expr=0, num_plus=0, num_minus=0, num_mult=0, num_div=0, num_remaind=0, larg_result=0, small_result=INT_MAX, intermediate_result=0;
    string menu_choice ="";

    //declaration of the vector of the pointers to the Expression class objects
    vector<Expression*> ListExpr;

    //menu-driven part of the code
    do
    {
      cout << "\nPlease enter a command (add, listall, listbyoperator, listsummary, and exit): ";
      getline(cin, menu_choice);

        //menu: adding new expression
        if (menu_choice == "add")
        {
            cout << "Please enter an expression: \n";

            string exp_name;
            string input;
            getline(cin, input);

            //checking if the input expression has an operator at all
            int temp_counter = 0;
            char array_of_operators[5] = {'+','-','*','/','%'};

            for (int i=0; i<5; i++)
            {
                string::difference_type n = count(input.begin(), input.end(), array_of_operators[i]);
                temp_counter+=n;
            }

            //checking if the input expression has one operator (expected case)
            if (temp_counter==1)
            {
                for (int i=0; i<5; i++)
                {
                    if (input.find(array_of_operators[i]) != string::npos)
                        {
                            //assigning of the found operator's character
                            operator_=array_of_operators[i];
                        }
                }

                //checking for how many '=' equal signs in the input expression
                string::difference_type n = count(input.begin(), input.end(), '=');

                //checking if the input expression has one equal sign (the case when the expression was entered in a potentially named format)
                if (n==1)
                {
                    //getting rid of the operator: returning the expression's name with an equal sign, and split operands around the operator as the vector of the strings
                    vector<string> seglist = tempFunct(input,operator_);
                    stringstream s;
                    //building string out of the "seglist" vector
                    for_each(begin(seglist), end(seglist), [&s](const string &elem) { s << "=" << elem; } );
                    string temp_str = s.str();

                    //getting rid of the equal sign:
                    seglist = tempFunct(temp_str,'=');

                    //checking if the first character of the first element of the vector is a letter (which is potentially the correctly input name of the expression)
                    if (isalpha(seglist[1][0]))
                        {
                            exp_name = seglist[1];
                        }
                    else
                        {
                            cout << "Invalid expression name. Name must start out with an alphabet character.";
                            continue;
                        }

                    //checking if the first and second operands are numbers
                    bool has_only_digits1 = (seglist[2].find_first_not_of( "0123456789" ) == string::npos);
                    bool has_only_digits2 = (seglist[3].find_first_not_of( "0123456789" ) == string::npos);

                    if (has_only_digits1 && has_only_digits2)
                        {
                        //assigning the operands to integer variables
                        operand1 = stoi(seglist[2]);
                        operand2 = stoi(seglist[3]);
                        }

                    else
                    {
                        cout << "Invalid operand. Number is expected.";
                        continue;
                    }

                    //creating the pointer to the NamedExpression class object and adding it to the vector of the pointers to the Expression class objects
                    Expression* expr = new NamedExpression(exp_name, operand1, operand2, operator_);
                    cout << expr->toString();
                    ListExpr.push_back(expr);

                    //invoking the function to get the result of the expression and data for expressions summary statistics
                    operator_funct(operand1, operand2, operator_, intermediate_result, tot_num_of_expr, num_plus, num_mult, num_minus, num_div, num_remaind);
                    //resultChecker - the function of checking the largest and smallest result among equations
                    resultChecker (larg_result, small_result, intermediate_result);
                }

                //checking if the input expression has no equal sign (the case when the expression was entered in a potentially unnamed format)
                else if(0==n)
                {

                    vector<string> seglist = tempFunct(input,operator_);

                    bool has_only_digits1 = false;
                    //returning if the expression's first operand is a digit
                    has_only_digits1 = isNumber(seglist[0]);
                    bool has_only_digits2 = false;
                    //returning if the expression's second operand is a digit
                    has_only_digits2 = isNumber(seglist[1]);

                    if (has_only_digits1 && has_only_digits2)
                    {
                        operand1 = stoi(seglist[0]);
                        operand2 = stoi(seglist[1]);

                        Expression* expr = new Expression(operand1, operand2, operator_);
                        cout << expr->toString();
                        ListExpr.push_back(expr);

                        operator_funct(operand1, operand2, operator_, intermediate_result, tot_num_of_expr, num_plus, num_mult, num_minus, num_div, num_remaind);
                        resultChecker (larg_result, small_result, intermediate_result);
                    }

                    //if one of the operands is not a digit
                    else
                    {
                        cout << "Invalid operand. Number is expected.";
                    }
                }
            }

            //checking if the input expression has no operator in the place where it was expected
            else if (temp_counter==0)
            {
                cout << "Unsupported expression operator.";
                continue;
            }

            //case when there was more than one operator input
            else
            {
                cout << "Entered more than one operator.";
                continue;
            }
        }

        //menu: displaying all the input expressions
        else if (menu_choice == "listall")
        {
        if (ListExpr.empty())
            {cout << "There is no expression.";}
        else
            {
            cout << "All expressions: \n";
            print_vector(ListExpr);
            }
        }

        //menu: the command will read in the operator and display only expressions with that given operator
        else if (menu_choice == "listbyoperator")
        {
            if (ListExpr.empty())
                {cout << "There is no expression.";}
            else
            {
                bool checker = false;
                cout << "Please enter the operator: " << endl;
                char oper_temp;
                cin >> oper_temp;
                cin.ignore();
                vector<char> list_operat = {'+','-','*','/','%'};
                //checking for the case when the user enters "listbyoperator" command and there is no matched expression
                if (find(list_operat.begin(), list_operat.end(), oper_temp) == list_operat.end())
                {
                    cout << "No expression is found with the operator: " << oper_temp << endl;
                }

                //output all expressions with only the given operator
                else
                {
                    for (int i=0; i<ListExpr.size(); i++)
                    {
                        if (ListExpr[i]->getOpeator() == oper_temp)
                        {
                            cout << ListExpr[i]->toString() << endl;
                        }
                    }
                }
            }
        }

        //menu: displaying the total number of expressions, the number of expressions for each operator, the largest and smallest expression values
        else if (menu_choice == "listsummary")
        {
            if (ListExpr.empty())
                {cout << "There is no expression.";}
            else
            {
                cout << "Total number of expressions: " << tot_num_of_expr
                << "\nNumber of '+' expressions:   " << num_plus
                << "\nNumber of '-' expressions:   " << num_minus
                << "\nNumber of '*' expressions:   " << num_mult
                << "\nNumber of '/' expressions:   " << num_div
                << "\nNumber of '%' expressions:   " << num_remaind
                << "\nLargest expression:          " << larg_result
                << "\nSmallest expression:         " << small_result << endl;
            }
        }

        else
        {
            //menu: exiting from the program
            if (menu_choice == "exit")
                {cout << "\n\nThank you. Goodbye.";}
            else
                {cout << "Unsupported command. Please try again.";}
        }
    } while (menu_choice != "exit");
}

//splitting the string around the character
vector<string> tempFunct(string string_, char operator_)
{
    stringstream ss(string_);
    string segment;
    vector<string> seglist;

    while(getline(ss, segment, operator_))
    {
        seglist.push_back(segment);
    }
    return seglist;
}

//checking if the string is a number
bool isNumber(string& str)
    {
        for (char const &c : str)
        {
            if (isdigit(c) == 0)
                {return false;}
        }
        return true;
    }

void print_vector(vector<Expression*> vect)
   {
       for(int i=0; i < vect.size(); i++)
       {
           cout << vect[i]->toString() << endl;
       }
   }

void addiTion (int num_1,  int num_2, char operator_, int& result)
    {
        result = num_1+num_2;
    }

void subTraction (int num_1,  int num_2, char operator_, int& result)
    {
        result = num_1-num_2;
    }

void mulTiplication (int num_1,  int num_2, char operator_, int& result)
    {
        result = num_1*num_2;
    }

void diviSion (int num_1,  int num_2, char operator_, int& result)
    {
        result = num_1/num_2;
    }

void reMainder (int num_1,  int num_2, char operator_, int& result)
    {
        result = num_1%num_2;
    }

void resultChecker (int& larg_result,  int& small_result, int& inter_result)
    {
        if (inter_result>larg_result)
            {
            larg_result = inter_result;
            }
        if (inter_result<small_result)
            {
            small_result = inter_result;
            }
    }

void operator_funct(int operand1, int operand2, char operator_, int &intermediate_result,
                    int &tot_num_of_expr, int &num_plus, int &num_mult, int &num_minus, int &num_div, int &num_remaind)
{
            if (operator_ == '+')
            {
                addiTion (operand1, operand2, operator_, intermediate_result);
                tot_num_of_expr++;
                num_plus++;
            }

            else if (operator_ == '-')
            {
                subTraction (operand1, operand2, operator_, intermediate_result);
                tot_num_of_expr++;
                num_minus++;
            }

            else if (operator_ == '*')
            {
                mulTiplication (operand1, operand2, operator_, intermediate_result);
                tot_num_of_expr++;
                num_mult++;
            }

            else if (operator_ == '/')
            {
                diviSion (operand1, operand2, operator_, intermediate_result);
                tot_num_of_expr++;
                num_div++;
            }

            else if (operator_ == '%')
            {
                reMainder (operand1, operand2, operator_, intermediate_result);
                tot_num_of_expr++;
                num_remaind++;
            }

            else
            {
                cout << "Smth goes wrong";
            }
}

int main ()
{
    ListNamedExpressions();
    return 0;
}
